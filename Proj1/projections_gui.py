import matplotlib.pyplot as plt
from tkinter import Canvas, Menu, Button, Toplevel, Label, Frame, Entry, StringVar ,NW , ALL


class ProjectionsGui:
    def __init__(self, parent):
        self.parent = parent
        hp_data =  calc_horizontal_projection(self.parent.pic)
        vp_data = calc_vertical_projection(self.parent.pic)
        fig = plt.figure(2)
        fig.clf()
        fig.suptitle('Projections')
        plt.subplot(211)    
        plt.plot(hp_data)
        plt.subplot(212)
        plt.plot(vp_data)
        plt.show()

def calc_vertical_projection(img):
    stack = []
    for row in img:
        stack.append(sum(pixel[0] == 0 for pixel in row))
    return stack

def calc_horizontal_projection(img):
    stack = []
    for col in img.transpose(1, 0, 2):
        stack.append(sum(pixel[0] == 0 for pixel in col))
    return stack


def calc_projections_for_iris(img, x_start, x_end, y_start, y_end):
    horiz = []
    vert = []
    
    for i, row in enumerate(img):
        if i in range(y_start, y_end):
            vert.append(sum(pixel[0] == 0 for pixel in row[x_start:x_end]))
        else:
            vert.append(0)
    
    for i, col in enumerate(img.transpose(1, 0, 2)):
        if i in range(x_start, x_end):
            horiz.append(sum(pixel[0] == 0 for pixel in col[y_start:y_end]))
        else:
            horiz.append(0)

    return horiz, vert
