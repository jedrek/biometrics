import numpy as np
import math

def brighten_darken(img, val):
    shape = img.shape
    flat_img = []
    for row in img:
        for pixel in row:
            for channel in pixel:
                new_val = int(channel + val)
                if new_val > 255: new_val=255
                elif new_val < 0: new_val=0
                flat_img.append(new_val)
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)  
    return new_img

def contrast(img, val):
    shape = img.shape
    flat_img = []
    for row in img:
        for pixel in row:
            for channel in pixel:
                new_val = int(channel * val)
                if new_val > 255: new_val=255
                elif new_val < 0: new_val=0
                flat_img.append(new_val)
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)
    return new_img
        
def normalize(img):
    #max channel value for normalization
    c_max = img.max()
    c_min = img.min()
    shape = img.shape
    flat_img = []
    for row in img:
        for pixel in row:
            for channel in pixel:
                new_val = int(255*((channel-c_min)/float(c_max-c_min)))
                flat_img.append(new_val)
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)
    return new_img

#𝛼 > 1 – exponentiation, darkening with greater variation of bright parts
#𝛼 < 1 – roots, brightening with greater variation of dark parts – gamma correction
def exp_normalize(img, alpha):
    #max channel value for normalization
    c_max = img.max()
    shape = img.shape
    flat_img = []
    for row in img:
        for pixel in row:
            for channel in pixel:
                new_val = 255*((channel/c_max)**alpha)
                flat_img.append(new_val)
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)
    return new_img

def log_normalize(img):
    #max channel value for normalization
    c_max = img.max()
    shape = img.shape
    flat_img = []
    for row in img:
        for pixel in row:
            for channel in pixel:
                new_val = 255*(math.log(1+channel)/math.log(1+c_max))
                flat_img.append(new_val)
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)
    return new_img

def negation(img):
    shape = img.shape
    flat_img = []
    for row in img:
        for pixel in row:
            for channel in pixel:
                new_val = 255 - channel
                if new_val > 255: new_val=255
                elif new_val < 0: new_val=0
                flat_img.append(new_val)
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)
    return new_img

#should be used on greyscale img, there is no intensity measuring
def threshold(img, t):
    shape = img.shape
    flat_img = []
    for row in img:
        for pixel in row:
            if pixel[0] < t: flat_img.extend([0,0,0])
            else: flat_img.extend([255, 255, 255])                
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)
    return new_img

def to_grayscale(img, option):
    shape = img.shape
    flat_img = []
    for row in img:
        for pixel in row:
            flat_img.extend(grayscale_funs[option](pixel))
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)
    return new_img

def grayscale_average_fun(pixel):
    pixel_val = 0
    for channel in pixel:
        pixel_val += channel
    c = int(pixel_val/3)
    return [c, c, c]

def grayscale_BT709_fun(pixel):
    c = pixel[0] * 0.2126 + pixel[1] * 0.7152 + pixel[2] * 0.0722
    return [c, c, c]

def grayscale_BT601_fun(pixel):
    c = pixel[0] * 0.299 + pixel[1] * 0.587 + pixel[2] * 0.114
    return [c, c, c]

def grayscale_desaturate_fun(pixel):
    c = (np.max(pixel) + np.min(pixel)) / 2
    return [c, c, c]

def grayscale_decompose_max_fun(pixel):
    c = np.max(pixel)
    return [c, c, c]

def grayscale_decompose_min_fun(pixel):
    c = np.min(pixel)
    return [c, c, c]

def grayscale_red_fun(pixel):
    c = pixel[0]
    return [c, c, c]

def grayscale_green_fun(pixel):
    c = pixel[1]
    return [c, c, c]

def grayscale_blue_fun(pixel):
    c = pixel[2]
    return [c, c, c]


grayscale_funs = {
    'Average': grayscale_average_fun,
    'BT.709': grayscale_BT709_fun,
    'BT.601': grayscale_BT601_fun,
    'Desaturate': grayscale_desaturate_fun,
    'Decompose max': grayscale_decompose_max_fun,
    'Decompose min': grayscale_decompose_min_fun,
    'Take Red': grayscale_red_fun,
    'Take Green': grayscale_green_fun,
    'Take Blue': grayscale_blue_fun
}