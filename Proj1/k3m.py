#k3m algoritm
import numpy as np
from thinning_common import *

def k3m(pic):
    M = ImgToArray(pic)
    shape = M.shape
    changes = True
    while changes:
        changes = False

        # phase 0 - mark borders
        phase = 0
        for i, row in enumerate(M):
            for j, cell in enumerate(row):
                if cell == 1:
                    w = GetWeight(getNeighbors(M, (i,j)))
                    if w in A[phase]:
                        M[i,j] = 2

        phase += 1
        while phase < 6:
            M_new = []  
            for i, row in enumerate(M):
                for j, cell in enumerate(row):
                    if cell == 2: #phase [1,5] - delete borders
                        w = GetWeight(getNeighbors(M, (i,j)))
                        if w in A[phase]:
                            M_new.append(0)
                            changes = True # a change has been made, continue looping
                        else:
                            M_new.append(M[i,j])
                    else:
                        M_new.append(M[i,j])
            M = np.array(M_new)
            M = M.reshape(shape)
            phase += 1

        # phase 6 - unmark borders
        for i, row in enumerate(M):
            for j, cell in enumerate(row):
                if cell == 2:
                    M[i,j] = 1

    # 1 pix phase
    M_new = []
    for i, row in enumerate(M):
        for j, cell in enumerate(row):
            if cell > 0:
                w = GetWeight(getNeighbors(M, (i,j)))
                if w in A['1pix']:
                    M_new.append(0)
                else:
                    M_new.append(1)
            else:
                M_new.append(M[i,j])
    M = np.array(M_new)
    M = M.reshape(shape)
   
    new_pic = GenerateImgFromArray(M)
    return new_pic

A = {
    0 : {3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60,
            62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135,
            143, 159, 191, 192, 193, 195, 199, 207, 223, 224,
            225, 227, 231, 239, 240, 241, 243, 247, 248, 249,
            251, 252, 253, 254},
    1 : { 7, 14, 28, 56, 112, 131, 193, 224  },
    2 : { 7, 14, 15, 28, 30, 56, 60, 112, 120, 131, 135,
            193, 195, 224, 225, 240 },
    3 : { 7, 14, 15, 28, 30, 31, 56, 60, 62, 112, 120,
            124, 131, 135, 143, 193, 195, 199, 224, 225, 227,
            240, 241, 248 },
    4 : { 7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120,
            124, 126, 131, 135, 143, 159, 193, 195, 199, 207,
            224, 225, 227, 231, 240, 241, 243, 248, 249, 252},
    5 : {7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120,
            124, 126, 131, 135, 143, 159, 191, 193, 195, 199,
            207, 224, 225, 227, 231, 239, 240, 241, 243, 248,
            249, 251, 252, 254},
    '1pix' : {3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56,
                60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131,
                135, 143, 159, 191, 192, 193, 195, 199, 207, 223,
                224, 225, 227, 231, 239, 240, 241, 243, 247, 248,
                249, 251, 252, 253, 254}
}