from tkinter import Canvas, Menu, Button, Toplevel, Scrollbar, Frame, NW, ALL, HORIZONTAL, X, VERTICAL, BOTTOM, RIGHT, LEFT, BOTH, Y
from tkinter.filedialog import askopenfilename, asksaveasfile, asksaveasfilename
from PIL import ImageTk, Image
import numpy as np

from ops_gui import OpsGui
from histograms_gui import HistogramsGui
from filters_gui import FiltersGui
from projections_gui import ProjectionsGui, calc_horizontal_projection, calc_vertical_projection
from morphology_gui import MorphologyGUI
from segmentation import detect_pupil, detect_pupil_and_iris
import pixel_functions as pf
from kmm import kmm
from k3m import k3m

# main window
class GUI:
    def __init__(self, root):
        self.root = root
        self.pic = None
        self.canvas_pic = None
        self.canvas = None
        self.ops_gui = None
        self.histograms_gui = None
        self.projections_gui = None
        self.filters_gui = None
        self.morphology_gui = None

    def init(self):
        self.frame = Frame(self.root, width=800, height=600)
        self.frame.pack(fill=BOTH, expand=True)
        self.canvas = Canvas(self.frame)
        self.create_menus()
        self.add_scrollbars()
        self.canvas.pack(side=LEFT, expand=True, fill=BOTH)
        self.root.mainloop()

    def create_menus(self):
        menubar = Menu(self.root)
        menubar.add_command(label="Open", command=self.load_file)
        menubar.add_command(label='Save', command=self.save_file)
        opmenu = Menu(menubar, tearoff=0)
        opmenu.add_command(label="Pixel functions", command=self.launch_ops_gui)
        opmenu.add_command(label="Filters", command=self.launch_filters_gui)
        opmenu.add_command(label="Morphology", command=self.launch_morphology_gui)
        menubar.add_cascade(label="Tools", menu=opmenu)
        opmenu2 = Menu(menubar, tearoff=0)
        opmenu2.add_command(label="Histogram", command=self.launch_histograms_gui)
        opmenu2.add_command(label="Projections", command=self.launch_projections_gui)
        menubar.add_cascade(label="Graphs", menu=opmenu2)
        opmenu3 = Menu(menubar, tearoff=0)
        opmenu3.add_command(label="Draw point in max Projections values", command=self.draw_pupil_center)
        opmenu3.add_command(label="Detect Pupil and Iris", command=self.detect_pupil_and_iris)
        opmenu3.add_command(label="Thinning KMM", command=self.thinning_kmm)
        opmenu3.add_command(label="Thinning K3M", command=self.thinning_k3m)
        menubar.add_cascade(label="Segmentation", menu=opmenu3)
        menubar.add_separator()
        menubar.add_command(label="Exit", command=self.root.destroy)
        self.root.config(menu=menubar)

    def launch_morphology_gui(self):
        if self.morphology_gui is None:
            self.morphology_gui = MorphologyGUI(self)
        else:
            self.morphology_gui.top.lift()

    def thinning_kmm(self):
        result = kmm(self.pic)
        self.update_pic(result, 'RGB')

    def thinning_k3m(self):
        result = k3m(self.pic)
        self.update_pic(result, 'RGB')

    def detect_pupil(self):
        point = detect_pupil(self.pic)
        self.canvas.create_oval(point[0], point[1], point[0]+5, point[1] + 5, fill="green")

    def detect_pupil_and_iris(self):
        # taking red leaves pale skin, and there are no red eyes, probably not great for brown eyes
        temp = pf.to_grayscale(self.pic, 'Take Red')
        temp = pf.exp_normalize(temp, 0.6)
        temp = pf.contrast(temp, 1.6)

        center, radius, pupil_center, pupil_radius = detect_pupil_and_iris(temp)
        if center is not None:
            self.canvas.create_oval(center[0]-radius, center[1]-radius, center[0]+radius, center[1] + radius, outline='#00ff00', width=3)
            if pupil_radius is not None:
                self.canvas.create_oval(pupil_center[0]-pupil_radius, pupil_center[1]-pupil_radius, pupil_center[0]+pupil_radius, pupil_center[1] + pupil_radius, outline='#ff0000', width=3)


    def draw_pupil_center(self):
        horizontal_proj = calc_horizontal_projection(self.pic)
        vertical_proj = calc_vertical_projection(self.pic)
        b = vertical_proj[::-1]
        last_vertical_idx = len(b) - np.argmax(b) - 1
        b = horizontal_proj[::-1]
        last_horizontal_idx = len(b) - np.argmax(b) - 1

        horizontal_proj = calc_horizontal_projection(self.pic)
        vertical_proj = calc_vertical_projection(self.pic)
        pupil_center = (
        int((np.argmax(horizontal_proj) + last_horizontal_idx) / 2),
        int((np.argmax(vertical_proj) + last_vertical_idx) / 2)
        )
        self.canvas.create_rectangle(pupil_center[0]-5, pupil_center[1]-5, pupil_center[0]+5, pupil_center[1]+5, fill="red")

    def launch_ops_gui(self):
        if self.ops_gui is None:
            self.ops_gui = OpsGui(self)
        else:
            self.ops_gui.top.lift()

    def launch_histograms_gui(self):
        # if self.histograms_gui is None:
        self.histograms_gui = HistogramsGui(self)

    def launch_projections_gui(self):
        # if self.projections_gui is None:
        self.projections_gui = ProjectionsGui(self)

    def launch_filters_gui(self):
        if self.filters_gui is None:
            self.filters_gui = FiltersGui(self)
        else:
            self.filters_gui.top.lift()

    def update_pic(self, new_pic, mode=None):
        self.pic = new_pic
        self._update_canvas(mode)

    # clear canvas and set current pic
    def _update_canvas(self, mode):
        self.canvas.delete("all")
        if mode is not None:
            i = Image.fromarray(self.pic, 'RGB')
        else:
            i = Image.fromarray(self.pic)
        self.canvas_pic = ImageTk.PhotoImage(i)
        # self.canvas.config(width=self.canvas_pic.width(), height=self.canvas_pic.height())
        self.canvas.create_image((0, 0), image=self.canvas_pic, anchor=NW)
        self.canvas.config(scrollregion=(
            0, 0, self.canvas_pic.width(), self.canvas_pic.height()))

    def add_scrollbars(self):
        hbar = Scrollbar(self.frame, orient=HORIZONTAL)
        hbar.pack(side=BOTTOM, fill=X)
        hbar.config(command=self.canvas.xview)
        vbar = Scrollbar(self.frame, orient=VERTICAL)
        vbar.pack(side=RIGHT, fill=Y)
        vbar.config(command=self.canvas.yview)
        self.canvas.config(xscrollcommand=hbar.set, yscrollcommand=vbar.set)

    def load_file(self):
        file = askopenfilename()
        if file is not ():
            pic = Image.open(file)
            if file.endswith(".bmp"):
                new_pic = self.image_from_fingerprint(pic)
            else:
                new_pic = np.asarray(pic)
            self.update_pic(new_pic)

    #we operate on images where there are 3 bits per color, so we adapt fingerprint images
    def image_from_fingerprint(self, pic):
        arr = np.asarray(pic)
        temp = np.array([[v, v, v] for v in arr.flatten()])
        new_size = list(arr.shape)
        new_size.append(3) # new size if [h, w, 3]
        temp.resize(new_size)
        return temp



    def save_file(self):
        file = asksaveasfilename()
        if file is None:
            return
        img = Image.fromarray(self.pic)
        img.save(file)
        
