import numpy as np

def generateMap(number):
    new_set = set()
    s = int(''.join(['1' for i in range(number)]), 2)
    l = number

    for i in range(8):
        n = (s << i)
        n = int(np.uint8(n))
        if i+l > 8:
            r = (i+l)%8
            b = ""
            for i in range(r):
                b += '1'
            n = n | int(b, 2)
        new_set.add(n)
    return new_set



def GenerateImgFromArray(M):
    img = []
    shape = list(M.shape)
    shape.append(3)
    for i, row in enumerate(M):
        for j, cell in enumerate(row):
            if cell > 0:
                img.append([0, 0, 0])
            else:
                img.append([255, 255, 255])
    arr = np.array(img, dtype='uint8')
    pic = np.resize(arr, shape)
    return pic


def GetWeight(neighbors):
    w = 0
    for i in neighbors:
        w +=  Weights[i]
    return w

def getNeighbors(M, p):
    n = []
    if p[0]-1 >= 0: # top
        if M[p[0]-1, p[1]] > 0:
            n.append(0)
    if p[0]+1 < M.shape[0]: # bottom
        if M[p[0]+1, p[1]] > 0:
            n.append(4)
    if p[1]-1 >= 0: # left
        if M[p[0], p[1]-1] > 0: 
            n.append(6)
    if p[1]+1 < M.shape[1]: # right
        if M[p[0], p[1]+1] > 0:
            n.append(2)
    if(p[0] - 1 >= 0 and p[1] - 1 >= 0): #top left corner
        if M[p[0] - 1, p[1] - 1] > 0:
            n.append(7)
    if(p[0] - 1 >= 0 and p[1] + 1 < M.shape[1]): #top right corner
        if M[p[0] - 1, p[1] + 1 ] > 0:
            n.append(1)
    if p[0] + 1 < M.shape[0] and p[1] - 1 >= 0: # bottom left corner
        if M[p[0] + 1, p[1] - 1] > 0:
            n.append(5)
    if p[0] + 1 < M.shape[0] and p[1] + 1 < M.shape[1]: # bottom right corner
        if M[p[0] + 1, p[1] + 1] > 0:
            n.append(3)
    return n

#does pixel stick to background
def GetSticky(M, p):
    sticky = []
    if p[0]-1 >= 0: # top
        if M[p[0]-1, p[1]] == 0:
            sticky.append(0)
    if p[0]+1 < M.shape[0]: # bottom
        if M[p[0]+1, p[1]] == 0:
            sticky.append(4)
    if p[1]-1 >= 0: # left
        if M[p[0], p[1]-1] == 0: 
            sticky.append(6)
    if p[1]+1 < M.shape[1]: # right
        if M[p[0], p[1]+1] == 0:
            sticky.append(2)
    return sticky

#if not sticky evaluate if is corner
def GetCorners(M, p):
    corners = []
    if(p[0] - 1 >= 0 and p[1] - 1 >= 0): #top left corner
        if M[p[0] - 1, p[1] - 1] == 0:
            corners.append(7)
    if(p[0] - 1 >= 0 and p[1] + 1 < M.shape[1]): #top right corner
        if M[p[0] - 1, p[1] + 1 ] == 0:
            corners.append(1)
    if p[0] + 1 < M.shape[0] and p[1] - 1 >= 0: # bottom left corner
        if M[p[0] + 1, p[1] - 1] == 0:
            corners.append(5)
    if p[0] + 1 < M.shape[0] and p[1] + 1 < M.shape[1]: # bottom right corner
        if M[p[0] + 1, p[1] + 1] == 0:
            corners.append(3)
    return corners

#translate image to 1's and 0's
def ImgToArray(pic):
    shape = list(pic.shape)
    shape.pop() # take only height and width
    p = np.resize(pic, shape).flatten()
    arr = []
    for row in pic:
        for pix in row:
            if pix[0] == 0:
                arr.append(1)
            else:
                arr.append(0)
    temp = np.array(arr)
    M = np.reshape(temp, shape)
    return M


# dictionary of values
# 7 0 1
# 6 X 2
# 5 4 3
Weights = {0: 1, 1: 2, 2: 4, 3: 8, 4: 16, 5: 32, 6: 64, 7: 128 }

