from tkinter import Canvas, Menu, Button, Toplevel, Label, OptionMenu, Frame, Entry, Grid, StringVar ,NW , ALL, N, S, E, W
from PIL import ImageTk,Image  
import numpy as np

import filters

#window for displaying and managing histograms
class FiltersGui:
    def __init__(self, parent):
        self.mask = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]
        self.parent = parent
        self.top = Toplevel(width=300, height=600)
        self.top.title("Filters")
        self.frame = Frame(self.top)
        self.frame.grid()
        #dropdown, kernel_size, button
        self.mask_type = StringVar(self.frame)
        choices = {'Low-pass', 'Gaussian', 'High-pass', 'Edge sobel 135',
                'Simple Edge', 'Custom'}
        self.mask_type.set('Custom')
        mask_menu = OptionMenu(self.frame, self.mask_type, *choices)
        mask_menu.grid(row=0, column=0)
        self.mask_grid = Frame(self.frame)
        
        self.kernel_size_e = Entry(self.frame)
        self.kernel_size_e.insert(0, 3)
        self.kernel_size_e.grid(row=0, column=1)
        Button(self.frame, text="Create kernel",
                command=self.create_mask).grid(row=0, column=2)
        Button(self.frame, text="Apply kernel",
                command=self.apply_mask).grid(row=2,column=1)
        self.top.protocol("WM_DELETE_WINDOW", self.on_closing)


    def on_closing(self):
        self.parent.filters_gui = None
        self.top.destroy()

    def create_mask(self):
        type = self.mask_type.get()
        k = int(self.kernel_size_e.get())
        self.mask = filters.create_mask(type, k)
        self.show_mask()
        
    #show the mask to edit freely
    def show_mask(self):
        self.mask_grid.destroy()
        self.mask_grid = Frame(self.frame)
        self.mask_grid.grid(sticky=N+S+E+W, column=1, row=1)

        self.gui_mask = []
        for row_index in range(len(self.mask)):
            Grid.rowconfigure(self.mask_grid, row_index, weight=1)
            temp = []
            for col_index in range(len(self.mask[0])):
                Grid.columnconfigure(self.mask_grid, col_index, weight=1)
                val = Entry(self.mask_grid) #create a button inside frame
                val.insert(0, self.mask[row_index][col_index]) 
                val.grid(row=row_index, column=col_index, sticky=N+S+E+W)  
                temp.append(val)
            self.gui_mask.append(temp)

    def apply_mask(self):
        self.load_mask_from_gui()
        new_pic = filters.apply_mask(self.mask, self.parent.pic)
        self.parent.update_pic(new_pic, 'RGB')

    def load_mask_from_gui(self):
        for i in range(len(self.gui_mask)):
            for j in range(len(self.gui_mask[0])):
                self.mask[i][j] = int(self.gui_mask[i][j].get())
    