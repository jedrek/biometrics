#file with morphological operations
# Dilation
# Erosion
import numpy as np

class StructuralElement:
    def __init__(self, arr, p):
        self.point = p
        self.arr = arr # array of 1/0's 
        struct_shape = (len(arr), len(arr[0]))
        temp = [[(i-p[0], j-p[1]) if arr[i, j]==1 else None for j in range(struct_shape[1])] for i in range(struct_shape[0])]
        self.struct_points = np.array(list(filter(lambda x: x is not None, [item for sublist in temp for item in sublist])))
        #self.struct_points contains a list of points(matrix coords) that are marked as 1's

    #get the dilated image, img - np.array
    def dilate(self, img):
        new_img = np.copy(img)
        shape =  img.shape
        for i, row in enumerate(img):
            for j, pixel in enumerate(row):
                if np.array_equal(pixel, [0,0,0]):
                    for temp in self.struct_points:
                        p = (i+temp[0], j+temp[1])
                        # treat pixels outside of image boundary as white
                        if p[0] >= 0 and p[0] < shape[0] and p[1] >= 0 and p[1] < shape[1]:
                            new_img[p[0], p[1]] = [0,0,0]
        return new_img

    # get the eroded image, img - np.array
    def erode(self, img):
        new_img = np.full(img.shape, 255, dtype='uint8') # create a full white image
        shape = img.shape
        for i, row in enumerate(img):
            for j, pixel in enumerate(row):
                add_current = True
                for temp in self.struct_points:
                    p = (i+temp[0], j+temp[1])
                    if p[0] >= 0 and p[0] < shape[0] and p[1] >= 0 and p[1] < shape[1] and np.array_equal(img[p[0], p[1]], [0,0,0]):
                        pass # if the pixel is inside image and is black, it's ok
                    else:
                        add_current = False
                        break
                if add_current:
                    new_img[i, j] = [0,0,0] # add a black pixel where the structural element allows for it
        return new_img

    