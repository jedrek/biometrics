from tkinter import Canvas, Menu, Button, Toplevel, Label, OptionMenu, Frame, Entry, Grid, StringVar ,NW , ALL, N, S, E, W, Label
from PIL import ImageTk,Image  
import numpy as np

from morphology import *


class MorphologyGUI:
    def __init__(self, parent):
        self.parent = parent
        self.grid = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]
        self.top = Toplevel(width=300, height=600)
        self.top.title("Morphology")
        self.frame = Frame(self.top)
        self.frame.grid()
        #dropdown, kernel_size, button
        
        self.morph_type = StringVar(self.frame)
        choices = {'Dilate', 'Erode'}
        self.morph_type.set('Erode')
        mask_menu = OptionMenu(self.frame, self.morph_type, *choices)
        mask_menu.grid(row=0, column=0)

        self.structural_element_grid = Frame(self.frame)
        
        self.kernel_size_e = Entry(self.frame)
        self.kernel_size_e.insert(0, 3)
        self.kernel_size_e.grid(row=0, column=1)
        Button(self.frame, text="Create structural element",
                command=self.create_mask).grid(row=0, column=2)
        Button(self.frame, text="Apply",
                command=self.morph).grid(row=2,column=2)
        self.point_x = Entry(self.frame)
        self.point_y = Entry(self.frame)
        Label(self.frame, text="Representative point X").grid(row=2)
        Label(self.frame, text="Representative point Y").grid(row=3)
        self.point_x.grid(row=2, column=1)
        self.point_y.grid(row=3, column=1)
        self.top.protocol("WM_DELETE_WINDOW", self.on_closing)

    def morph(self):
        type = self.morph_type.get()
        self.load_mask_from_gui()
        point = (int(self.point_x.get()), int(self.point_y.get()))
        structural_element = StructuralElement(np.array(self.grid), point)
        if type == 'Dilate':
            new_pic = structural_element.dilate(self.parent.pic)
        else:
            new_pic = structural_element.erode(self.parent.pic)
        self.parent.update_pic(new_pic, 'RGB')


    def on_closing(self):
        self.parent.morphology_gui = None
        self.top.destroy()

    def create_mask(self):
        k = int(self.kernel_size_e.get())
        self.grid = [[1 for col in range(k)] for row in range(k)]
        self.show_mask()

    def load_mask_from_gui(self):
            for i in range(len(self.gui_grid)):
                for j in range(len(self.gui_grid[0])):
                    self.grid[i][j] = int(self.gui_grid[i][j].get())

    def show_mask(self):
        self.structural_element_grid.destroy()
        self.structural_element_grid = Frame(self.frame)
        self.structural_element_grid.grid(sticky=N+S+E+W, column=1, row=1)

        self.gui_grid = []
        for row_index in range(len(self.grid)):
            Grid.rowconfigure(self.structural_element_grid, row_index, weight=1)
            temp = []
            for col_index in range(len(self.grid[0])):
                Grid.columnconfigure(self.structural_element_grid, col_index, weight=1)
                val = Entry(self.structural_element_grid) #create a button inside frame
                val.insert(0, self.grid[row_index][col_index]) 
                val.grid(row=row_index, column=col_index, sticky=N+S+E+W)  
                temp.append(val)
            self.gui_grid.append(temp)
