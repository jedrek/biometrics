from tkinter import Canvas, Menu, Button, Toplevel, Label, Frame, Entry, StringVar ,NW , ALL
from PIL import ImageTk,Image 
import matplotlib.pyplot as plt 
import numpy as np
import math
import pixel_functions


#window for displaying and managing histograms
class HistogramsGui:
    def __init__(self, parent):
        self.parent = parent
        self.show_plots()

    def show_plots(self):
        colors = ['r', 'g', 'b']
        hist_data = self.calc_histogram(1)
        fig = plt.figure(1)
        fig.clf()
        fig.suptitle('Histogram')
        plt.subplot(311)
        for col, data in zip(colors, hist_data):
            plt.plot(data, color = col)
            plt.xlim([0,256])
        frame1 = plt.gca()
        frame1.axes.yaxis.set_ticklabels([])
        plt.show()

    #picture needs to be grayscale
    def calc_histogram(self, interval_size):
        interval_count = int(256/interval_size)
        hist_r = [0]*interval_count
        hist_g = [0]*interval_count
        hist_b = [0]*interval_count
        #calculate histogram
        for row in self.parent.pic:
            for pixel in row:
                #probably should calc intensity here
                hist_r[int((pixel[0])/interval_size)] += 1
                hist_g[int((pixel[1])/interval_size)] += 1
                hist_b[int((pixel[2])/interval_size)] += 1
        return (hist_r, hist_g, hist_b)

   


        