import numpy as np
from thinning_common import *
# KMM and K3M implementations 



def kmm(pic):
    M = ImgToArray(pic)
    shape = M.shape
    skeleton = False

    max_iter = 10
    iter = 0

    while not skeleton and iter < max_iter:
        iter += 1
        skeleton = True
        #mark sticky and corners
        for i, row in enumerate(M):
            for j, val in enumerate(row):
                if val > 0:
                    sticky = GetSticky(M, (i,j))
                    corners = GetCorners(M, (i,j))
                    if  len(sticky) > 0:
                        M[i,j] = 2
                    else:
                        if len(corners) > 0:
                            M[i,j] = 3
        
        # mark 4's
        for i, row in enumerate(M):
            for j, val in enumerate(row):
                if (val == 2 or val == 3) and GetWeight(getNeighbors(M, (i,j))) in MAP4: #mark as 4
                    M[i,j] = 4

        #delete 4's
        for i, row in enumerate(M):
            for j, val in enumerate(row):
                if val == 4:
                    skeleton = False
                    M[i,j] = 0

        N = 2
        M_new = []
        for i, row in enumerate(M):
            for j, val in enumerate(row):
                if val == N:
                    weight = GetWeight(getNeighbors(M, (i,j)))
                    if weight in MAP4 and weight in DeletionTable:
                        M_new.append(0)
                        skeleton = False
                    else:
                        M_new.append(1)
                else:
                    M_new.append(M[i,j])
        M = np.array(M_new)
        M = M.reshape(shape)

        N = 3
        M_new = []
        for i, row in enumerate(M):
            for j, val in enumerate(row):
                if val == N:
                    weight = GetWeight(getNeighbors(M, (i,j)))
                    if weight in MAP4 and weight in DeletionTable:
                        M_new.append(0)
                        skeleton = False
                    else:
                        M_new.append(1)
                else:
                    # pass
                    M_new.append(M[i,j])
        M = np.array(M_new)
        M = M.reshape(shape)

    new_pic = GenerateImgFromArray(M)
    return new_pic


#to mark 4's
# neighbr_count - number of stick nighbours to generate map for
MAP4 = set.union(generateMap(2), generateMap(3), generateMap(4))

DeletionTable = {
    3, 5, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28, 29, 30, 31, 48, 52, 53, 54, 55, 56, 60, 61, 62, 63, 65, 67, 69, 71, 77, 79, 80,
    81, 83, 84, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, 97, 99, 101, 103, 109, 111, 112,
    113, 115, 116, 117, 118, 119, 120, 121, 123, 124, 125, 126, 127, 131, 133, 135, 141,
    143, 149, 151, 157, 159, 181, 183, 189, 191, 192, 193, 195, 197, 199, 205, 207,
    208, 209, 211, 212, 213, 214, 215, 216, 217, 219, 220, 221, 222, 223, 224, 225, 227,
    229, 231, 237, 239, 240, 241, 243, 244, 245, 246, 247, 248, 249, 251, 252, 253, 254, 255
}