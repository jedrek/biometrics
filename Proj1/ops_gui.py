from tkinter import Canvas, Menu, Button, Toplevel, Label, Frame, Entry, StringVar, OptionMenu, NW, ALL
from PIL import ImageTk, Image
import numpy as np
import pixel_functions as pf


# window for pixel operations management
class OpsGui:
    def __init__(self, parent):
        self.parent = parent
        # init window
        self.top = Toplevel(width=300, height=600)
        self.top.title("Pixel operations")
        self.frame = Frame(self.top)
        self.frame.grid()
        # dropdonw with grayscale transition options
        Button(self.frame, text="To Grayscale",
               command=self.to_grayscale).grid(row=0)
        Button(self.frame, text="Brighten/Darken",
               command=self.brighten_darken).grid(row=1)
        Button(self.frame, text="Contrast",
               command=self.contrast).grid(row=2)
        Button(self.frame, text="Negation",
               command=self.negation).grid(row=3)
        Button(self.frame, text="Thresholding",
               command=self.threshold).grid(row=4)
        Button(self.frame, text="Normalize",
                command=self.normalize).grid(row=5)
        Button(self.frame, text="Exponential Normalize",
                command=self.exp_normalize).grid(row=6)
        Button(self.frame, text="Logarithmic Normalize",
                command=self.log_normalize).grid(row=7)
        self.brighten_darken_e = Entry(self.frame)
        self.contrast_e = Entry(self.frame)
        self.threshold_e = Entry(self.frame)
        self.grayscale_opt = StringVar(self.frame)
        self.exp_normalize_alpha_e = Entry(self.frame)
        # bt.709 - Gray = (Red * 0.2126 + Green * 0.7152 + Blue * 0.0722)
        # bt.601 - Gray = (Red * 0.299 + Green * 0.587 + Blue * 0.114)
        # desaturation - Gray = ( Max(Red, Green, Blue) + Min(Red, Green, Blue) ) / 2
        choices = {'Average', 'BT.709', 'BT.601',
                   'Desaturate', 'Decompose max', 'Decompose min',
                   'Take Red', 'Take Green', 'Take Blue'}
        self.grayscale_opt.set('Average')
        grayscale_dropdown_menu = OptionMenu(self.frame, self.grayscale_opt, *choices)
        grayscale_dropdown_menu.grid(row=0, column=1)
        self.brighten_darken_e.grid(row=1, column=1)
        self.contrast_e.grid(row=2, column=1)
        self.threshold_e.grid(row=4, column=1)
        self.exp_normalize_alpha_e.grid(row=6, column=1)
        self.top.protocol("WM_DELETE_WINDOW", self.on_closing)

    def on_closing(self):
        self.parent.ops_gui = None
        self.top.destroy()

    def brighten_darken(self):
        val = float(self.brighten_darken_e.get())
        new_pic = pf.brighten_darken(self.parent.pic, val)
        self.parent.update_pic(new_pic, 'RGB')

    def contrast(self):
        val = float(self.contrast_e.get())
        new_pic = pf.contrast(self.parent.pic, val)
        self.parent.update_pic(new_pic, 'RGB')

    def negation(self):
        new_pic = pf.negation(self.parent.pic)
        self.parent.update_pic(new_pic, 'RGB')

    def to_grayscale(self):
        option = self.grayscale_opt.get()
        new_pic = pf.to_grayscale(self.parent.pic, option)
        self.parent.update_pic(new_pic, 'RGB')

    def threshold(self):
        t = int(self.threshold_e.get())
        new_pic = pf.threshold(self.parent.pic, t)
        self.parent.update_pic(new_pic, 'RGB')
    
    def normalize(self):
        new_pic = pf.normalize(self.parent.pic)
        self.parent.update_pic(new_pic, 'RGB')

    def exp_normalize(self):
        alpha = float(self.exp_normalize_alpha_e.get())
        new_pic = pf.exp_normalize(self.parent.pic, alpha)
        self.parent.update_pic(new_pic, 'RGB')

    def log_normalize(self):
        new_pic = pf.log_normalize(self.parent.pic)
        self.parent.update_pic(new_pic, 'RGB')

    