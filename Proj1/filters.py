import math
import numpy as np

def create_mask(type, k):
    if (type,k) in masks: # if mask in predefined
        mask = masks[(type,k)]
    elif type in masks: #generate if not predefined
        mask = masks[type](k)
    else:
        raise ValueError('Unsupported mask type')  

    return mask


def apply_mask(mask, img):
    shape = img.shape
    mask_weight = sum(sum(mask, []))
    if mask_weight == 0: mask_weight = 1

    m_h = [ i for i in range(-math.floor(len(mask)/2), math.ceil(len(mask)/2))]
    m_w = [ i for i in range(-math.floor(len(mask[0])/2), math.ceil(len(mask[0])/2))]

    #if there is no middle pixel treat the current as a top left
    if len(mask)%2==0 or len(mask[0])%2==0:
        m_h = [ i for i in range(len(mask))]
        m_w = [ i for i in range(len(mask[0]))]

    flat_img = []
    for r in range(shape[0]):
        for c in range(shape[1]):
            new_pixel = [0]*3
            for i_m, i in enumerate(m_h):
                for j_m, j in enumerate(m_w):
                    for chnl in range(3):
                        idx_x = r+i
                        idx_y = c+j
                        #maybe instead of performing this check all the time
                        #just increase the image size and fill it with border pixels
                        if idx_x >= shape[0]: idx_x = shape[0]-1
                        elif idx_x < 0: idx_x = 0
                        if idx_y >= shape[1]: idx_y = shape[1]-1
                        elif idx_y < 0: idx_y = 0
                        new_pixel[chnl] += mask[i_m][j_m] * img[idx_x][idx_y][chnl]
            #recreating the array 3 times just to obtain good values
            new_pixel[:] = [c/mask_weight for c in new_pixel]
            new_pixel[:] = [abs(c) if c <= 255 else 255 for c in new_pixel]

            flat_img.append(new_pixel)
    new_img = np.array(flat_img, dtype='uint8').reshape(shape)
    return new_img


masks = {
    ('Low-pass', 3): [[1, 1, 1], [1, 1, 1], [1, 1, 1]],
    ('Gaussian', 3): [[1, 2, 1], [2, 4, 2], [1, 2, 1]],
    ('High-pass', 3): [[0, -1, 0], [-1, 4, -1], [0, -1, 0]],
    ('Edge sobel 135', 3): [[2, 1, 0], [1, 0, -1], [0, -1, -2]],
    ('Simple Edge', 2): [[1, 0], [0, -1]],
    'Custom': lambda k:[[0 for col in range(k)] for row in range(k)]
}