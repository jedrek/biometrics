# segmentation algorithm
import pixel_functions as pf
from morphology import StructuralElement
from projections_gui import calc_horizontal_projection, calc_vertical_projection, calc_projections_for_iris
import numpy as np
import cv2


#returns the center point and the radius
def detect_pupil(img):
    arr = np.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
    struct = StructuralElement(arr, (1, 1))

    # perform some operations and detect
    temp_img = pf.to_grayscale(img, 'BT.601')
    temp_img = pf.normalize(temp_img)
    temp_img = pf.threshold(temp_img, 15)
    temp_img = struct.erode(temp_img)
    temp_img = struct.erode(temp_img)
    temp_img = struct.dilate(temp_img)

    horizontal_proj = calc_horizontal_projection(temp_img)
    vertical_proj = calc_vertical_projection(temp_img)
    
    b = vertical_proj[::-1]
    last_vertical_idx = len(b) - np.argmax(b) - 1
    b = horizontal_proj[::-1]
    last_horizontal_idx = len(b) - np.argmax(b) - 1

    pupil_center = (
        int((np.argmax(vertical_proj) + last_vertical_idx) / 2),
        int((np.argmax(horizontal_proj) + last_horizontal_idx) / 2)
        )

    return pupil_center


def detect_pupil_and_iris(img):
    # img here should be already passed in grayscale
    gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    circles = cv2.HoughCircles(gray_img, cv2.HOUGH_GRADIENT, 2, 9999,
    param1=200,param2=100,minRadius=70,maxRadius=160)
    iris_center = None
    if circles is not None:
        circles = np.around(circles).astype('uint16')
        for i in circles[0,:]:
            # draw the outer circle
            iris_center, iris_radius = (i[0],i[1]),i[2]


    if iris_center is not None:
        arr = np.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
        struct = StructuralElement(arr, (1, 1))
        # threshold 100 should be fine
        binarized_img = pf.threshold(img, 100)
        #closing operation
        binarized_img = struct.erode(binarized_img)
        binarized_img = struct.dilate(binarized_img)

        # horizontal_proj = calc_horizontal_projection(binarized_img)
        # vertical_proj = calc_vertical_projection(binarized_img)

        horizontal_proj, vertical_proj = calc_projections_for_iris(binarized_img,
            iris_center[0]-int(iris_radius/2), iris_center[0] + int(iris_radius/2),
            iris_center[1]-int(iris_radius/2), iris_center[1] + int(iris_radius/2)
            )

        pupil_x_start, pupil_x_end, pupil_y_start, pupil_y_end = (None, None, None, None)
        # iris_radius/3 here because pupil can't cover the whole iris and 2/3 is enough imo
        for x in range(iris_center[1]-int(iris_radius/2), iris_center[1] + int(iris_radius/2)):
            if vertical_proj[x] > 5:
                pupil_x_start = x
        for x in reversed(range(iris_center[1]-int(iris_radius/2), iris_center[1] + int(iris_radius/2))):
            if vertical_proj[x] > 5:
                pupil_x_end = x
        for y in range(iris_center[0]-int(iris_radius/2), iris_center[0] + int(iris_radius/2)):
            if horizontal_proj[y] > 5:
                pupil_y_start = y
        for y in reversed(range(iris_center[0]-int(iris_radius/2), iris_center[0] + int(iris_radius/2))):
            if horizontal_proj[y] > 5:
                pupil_y_end = y

        if any(el is None for el in (pupil_x_start, pupil_x_end, pupil_y_start, pupil_y_end)):
            return iris_center, iris_radius, None, None

        pupil_radius = ((pupil_x_end-pupil_x_start) + (pupil_y_end - pupil_y_start)) / 4
        pupil_center = (pupil_y_start + pupil_radius, pupil_x_start + pupil_radius)

    return iris_center, iris_radius, pupil_center, pupil_radius
